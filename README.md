# Helm (in Alpine) Container

A minimal tools container.. So I don't have to install Helm.

## Build

```shell
./build.sh
```

## Push to Dockerhub

```shell
./build.sh
```

## Run

```shell
docker run --rm figaw/helm:3.0.2-alpine-3.11 help
```

## Alias

```shell
alias helm='docker run --rm \
    -v $KUBECONFIG:/root/.kube/config \
    -v `pwd`:/mnt/helm \
    -w /mnt/helm \
    figaw/helm:3.0.2-alpine-3.11'
```

> NB: substitute `$KUBECONFIG` with the location of your `kubeconfig`,
> if you're not using the environment variable.
