#!/bin/sh

docker build \
    -t figaw/helm:3.0.2-alpine-3.11 \
    -t figaw/helm:3.0-alpine-3.11 \
    -t figaw/helm:3-alpine-3.11 \
    -t figaw/helm:3.0.2-alpine \
    -t figaw/helm:3.0-alpine \
    -t figaw/helm:3-alpine \
    .
