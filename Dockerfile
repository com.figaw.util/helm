FROM alpine:3.11.3 as builder

# Extract the helm archive <replace with new helm binary..>
ADD helm-v3.0.2-linux-amd64.tar.gz /root/

FROM alpine:3.11.3

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

# Copy the helm binary to a clean alpine image
COPY --from=builder /root/linux-amd64/helm /usr/local/bin/helm

RUN helm repo add stable https://kubernetes-charts.storage.googleapis.com/

VOLUME [ "/root/.helm", "/root/.kube" ]

ENTRYPOINT [ "helm" ]
